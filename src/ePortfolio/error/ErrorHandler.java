/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  @Author: Olawale Onigemo
 */
package ePortfolio.error;

import ePortfolio.view.ePortfolioMakerView;
import javafx.scene.control.Alert;
import properties_manager.PropertiesManager;

/**
 *
 * @author WaleO
 */
/**
 * This class provides error messages to the user when the occur. Note that
 * error messages should be retrieved from language-dependent XML files and
 * should have custom messages that are different depending on the type of error
 * so as to be informative concerning what went wrong.
 *
 * @author McKilla Gorilla & _____________
 * @co_author Olawale Onigemo
 */
public class ErrorHandler {

    // APP UI

    private ePortfolioMakerView ui;

    // KEEP THE APP UI FOR LATER
    public ErrorHandler(ePortfolioMakerView initUI) {
        ui = initUI;
    }

    /**
     * This method provides all error feedback. It gets the feedback text, which
     * changes depending on the type of error, and presents it to the user in a
     * dialog box.
     *
     * @param errorDialogTitle
     * @param errorDialogMessage
     */
    public void processError(String errorDialogTitle, String errorDialogMessage) {
        // GET THE FEEDBACK TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //String errorFeedbackText = props.getProperty(errorType);

        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        //Send the error message to the error handler. 
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText(errorDialogTitle);
        alert.setContentText(errorDialogMessage);

        alert.showAndWait();
    }
}