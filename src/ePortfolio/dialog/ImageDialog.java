/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.dialog;

import ePortfolio.view.ePortfolioMakerView;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author WaleO
 */
public class ImageDialog {

    VBox contentBox;

    //HBox to put labels and textfields together.
    HBox urlBox;
    HBox widthBox;
    HBox heightBox;
    HBox floatBox;
    HBox buttonBox;

    Button okButton;
    Button cancel;
     Button urlButton;

    String newText;
    ePortfolioMakerView ui;
    int index;

    //Different textfields for the image.
    TextField url;
    TextField width;
    TextField height;
    TextField floater;

    //The labels for the text fields 
   
    Label widthLabel;
    Label heightLabel;
    Label floatLabel;

    public ImageDialog(ePortfolioMakerView view) {
        contentBox = new VBox();
        urlBox = new HBox();
        widthBox = new HBox();
        heightBox = new HBox();
        floatBox = new HBox();
        buttonBox = new HBox();

        urlButton = new Button();
        widthLabel = new Label();
        heightLabel = new Label();
        floatLabel = new Label();

        okButton = new Button();
        cancel = new Button();
        ui = view;
        index = 0;

        url = new TextField();
        width = new TextField();
        height = new TextField();
        floater = new TextField();
    }

    public void processImage() {

        contentBox = new VBox();
        urlBox = new HBox();
        widthBox = new HBox();
        heightBox = new HBox();
        floatBox = new HBox();
        buttonBox = new HBox();
        
        url = new TextField();
        width = new TextField();
        height = new TextField();
        floater = new TextField();

        urlButton = new Button();
        widthLabel = new Label();
        heightLabel = new Label();
        floatLabel = new Label();
        urlButton = new Button();

       
        okButton.setText("Ok");
        cancel.setText("Cancel");

        //Set up the Labels 
        urlButton.setText("URl      ");
        widthLabel.setText("Width  ");
        heightLabel.setText("Height ");
        floatLabel.setText("Label   ");
        
        //Scale Url button
        

        //Add all the labels and boxes to respective boxes (HBoxs).
        urlBox.getChildren().add(urlButton);
        urlBox.getChildren().add(url);

        widthBox.getChildren().add(widthLabel);
        widthBox.getChildren().add(width);

        heightBox.getChildren().add(heightLabel);
        heightBox.getChildren().add(height);

        floatBox.getChildren().add(floatLabel);
        floatBox.getChildren().add(floater);

        buttonBox.getChildren().add(okButton);
        buttonBox.getChildren().add(cancel);
        //Add the Hboxs to the VBox(The entire content)
        contentBox.getChildren().add(urlBox);
        contentBox.getChildren().add(widthBox);
        contentBox.getChildren().add(heightBox);
        contentBox.getChildren().add(floatBox);
        contentBox.getChildren().add(buttonBox);
        Stage stage = new Stage();
        stage.setTitle("Edit Image Settings");
        Scene scene = new Scene(contentBox, 300, 300);
        stage.setScene(scene);
        stage.show();
    }
}
