/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.dialog;

import ePortfolio.component.SlideShowComponent;
import ePortfolio.model.Slide;
import ePortfolio.view.SlideEditView;
import ePortfolio.view.ePortfolioMakerView;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_VERTICALBAR;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS;
import static eportfoliomaker.ePortfolioConstants.ICON_ADD_PAGE;
import static eportfoliomaker.ePortfolioConstants.ICON_NEXT;
import static eportfoliomaker.ePortfolioConstants.ICON_PREVIOUS;
import static eportfoliomaker.ePortfolioConstants.ICON_REMOVE_PAGE;
import static eportfoliomaker.ePortfolioConstants.STYLE_SHEET_UI;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author WaleO
 */
public class SlideShowDialog {

    BorderPane pane = new BorderPane();
    HBox workSpace = new HBox();
    VBox slideToolBars = new VBox();
    VBox slideEditorPane = new VBox();
    ePortfolioMakerView ui; 
    SlideShowComponent slideShowModel;

    Button addButton;
    Button removeSlideButton;
    Button upSlideButton;
    Button downSlideButton;
 
    public SlideShowDialog(ePortfolioMakerView view)
    {
        pane = new BorderPane();
        workSpace = new HBox();
        slideToolBars = new VBox();
        slideEditorPane = new VBox();
        ui = view; 
    }
    public void processMakeSlideShow() {
        pane = new BorderPane();
        workSpace = new HBox();
        slideToolBars = new VBox();
        slideEditorPane = new VBox();


        slideToolBars.getStyleClass().add(CSS_CLASS_VERTICALBAR);

        addButton = ui.initChildButton(slideToolBars, ICON_ADD_PAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        removeSlideButton = ui.initChildButton(slideToolBars, ICON_REMOVE_PAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        upSlideButton = ui.initChildButton(slideToolBars, ICON_NEXT, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        downSlideButton = ui.initChildButton(slideToolBars, ICON_PREVIOUS, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);

        ScrollPane slidesEditorScrollPane = new ScrollPane(slideEditorPane);
        workSpace.getChildren().add(slideToolBars);
        workSpace.getChildren().add(slidesEditorScrollPane);

        pane.setLeft(workSpace);

        Stage stage = new Stage();
        stage.setTitle("SlideShow");
        Scene scene = new Scene(pane, 500, 500);
        scene.getStylesheets().add(STYLE_SHEET_UI);

        stage.setScene(scene);
        stage.show();

    }
    
    public void reloadSlideShowMakerPane(SlideShowComponent slideShowToLoad)
    {
        int i = 0;
        slideEditorPane.getChildren().clear();
        for (Slide slide : slideShowToLoad.getSlides()) {
            slideShowModel = slideShowToLoad;
            SlideEditView slideEditor = new SlideEditView(slide, slideShowModel, this);
            slideEditorPane.getChildren().add(slideEditor);

            /**
             * Compares the slide to the selected slide. Will rehighlight the
             * selected slide if it is true First we must check to see if a
             * slide in the slide show has been selected.
             */
            if (slideShowModel.isSlideSelected()) {
                if (slideShowModel.testSlide(slide)) {
                    slideShowModel.setSelectedSlide(slide);
                    slideEditor.setEffect(new DropShadow(50, Color.BLUE));
                    removeSlideButton.setDisable(false);
                    upSlideButton.setDisable(false);
                    downSlideButton.setDisable(false);
                }

            } else {
                slideEditor.setEffect(null);
                removeSlideButton.setDisable(true);
                upSlideButton.setDisable(true);
                downSlideButton.setDisable(true);
            }
        }
    }
}
