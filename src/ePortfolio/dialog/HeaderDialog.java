/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.dialog;

import ePortfolio.component.Header;
import ePortfolio.model.ePortfolioModel;
import ePortfolio.view.ePortfolioMakerView;
import java.util.Optional;
import static javafx.application.Platform.exit;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This class will provide necessary dialog for when a text component is being
 * edited. The text point is edited when it is been clicked on once like a
 * button.
 *
 * @author WaleO
 */
public class HeaderDialog {

    HBox content;
    VBox fieldBox;
    Button okButton;
    Button cancel;
    String newText;
    ePortfolioMakerView ui;
    int index;

    public HeaderDialog() {
        content = new HBox();
        fieldBox = new VBox();
        okButton = new Button();
        cancel = new Button();
        index = 0;

    }

    /*
     *@param oldText The old text to be change or kept
     *@param view  The ePortfolioView to be updated as changes are made or have not be made 
     */
    public void editHeader(String oldText, ePortfolioMakerView view) {
        TextField txtField = new TextField();
        ui = view;

        txtField.setText(oldText);
        content = new HBox();
        fieldBox = new VBox();
        okButton = new Button();
        cancel = new Button();
        okButton.setText("Ok");
        cancel.setText("Cancel");

        content.getChildren().add(txtField);
        content.getChildren().add(okButton);
        content.getChildren().add(cancel);
        fieldBox.getChildren().add(content);
        index = ui.getEportfolio().getSelectedIndex();

        Stage stage = new Stage();
        stage.setTitle("Edit Header");
        Scene scene = new Scene(fieldBox, 300, 300);
        stage.setScene(scene);
        stage.show();

        //Handles the adding of list elements. 
        okButton.setOnAction(e -> {
            newText = txtField.getText();
            //Get the index of the selected textView component 
            if (ui.getEportfolio().getComponents().get(index).getComponentType().equals("Header")) 
            {
                //If the selected textView component is header, 
                //Change the header of that selected header component. 
                ((Header) ui.getEportfolio().getSelectedComponent()).setHeader(newText);
            }

            //Reload the eportfolio
            ui.reloadEportfolioPane(ui.getEportfolio());
            stage.close();
        });

        //Leave the stage with no changes. 
        cancel.setOnAction(e -> {
            stage.close();
        });

    }

}
