/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.dialog;

import ePortfolio.component.ParagraphComponent;
import ePortfolio.view.ePortfolioMakerView;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author WaleO
 */
public class ParagraphDialog {

    Button okButton;
    Button cancel;
    String newText;
    TextArea text;
    HBox buttonBox;
    int index;
    ePortfolioMakerView ui;

    public ParagraphDialog() {
        text = new TextArea();
        buttonBox = new HBox();
        okButton = new Button();
        cancel = new Button();
        buttonBox = new HBox();
        index = 0;
    }

    public String editParagraph(String oldParagraph, ePortfolioMakerView view) {
        //Allows the user to enter text in the form of a paragraph. 
        ui = view;
        
        text = new TextArea();
        text.setText(oldParagraph);
        buttonBox = new HBox();
        okButton = new Button();
        cancel = new Button();
        okButton.setText("Ok");
        cancel.setText("Cancel");
        index = ui.getEportfolio().getSelectedIndex();

        //Display the enter paragraph as a hyperlink
        Button display = new Button("Link all");
        Button someText = new Button("Select Text");

        display.setOnAction(e -> {
            Stage stage2 = new Stage();
            newText = text.getText();

            stage2.setTitle("HyperLinked Edit");
            stage2.setWidth(200);
            stage2.setHeight(200);
            Scene scene = new Scene(new Group());
            VBox root = new VBox();
            Hyperlink link2 = new Hyperlink(newText);

            newText = text.getText();

            stage2.setTitle("Paragraph Editing");
            stage2.setWidth(500);
            stage2.setHeight(500);

            root.getChildren().addAll(link2);
            scene.setRoot(root);

            stage2.setScene(scene);
            stage2.show();

        });

        buttonBox.getChildren().add(display);
        buttonBox.getChildren().add(okButton);
        buttonBox.getChildren().add(cancel);
        BorderPane pane = new BorderPane();
        pane.setCenter(text);
        pane.setBottom(buttonBox);
        Stage stage = new Stage();
        stage.setTitle("Paragraph");
        Scene scene = new Scene(pane, 200, 220);
        stage.setScene(scene);

        stage.show();

        okButton.setOnAction(e -> {
            newText = text.getText();
            if (ui.getEportfolio().getComponents().get(index).getComponentType().equals("Paragraph")) {
                //If the selected textView component is header, 
                //Change the header of that selected header component. 
                ((ParagraphComponent) ui.getEportfolio().getSelectedComponent()).setParagraph(newText);
            }

            //Reload the eportfolio
            ui.reloadEportfolioPane(ui.getEportfolio());
            stage.close();
        });

        cancel.setOnAction(e -> {
            newText = text.getText();
            stage.close();
        });
        return newText;
    }

}
