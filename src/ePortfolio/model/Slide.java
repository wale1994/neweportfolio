/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 @Author: Olawale Onigemo
 */
package ePortfolio.model;

/**
 *
 * @author WaleO
 */
public class Slide {

    String imageFileName;
    String imagePath;
    String text;

    /**
     * Constructor, it initializes all slide data.
     *
     * @param initImageFileName File name of the image.
     *
     * @param initImagePath File path for the image.
     *
     */
    public Slide(String initImageFileName, String initImagePath, String caption) {
        imageFileName = initImageFileName;
        imagePath = initImagePath;
        text = caption;

    }

    // ACCESSOR METHODS
    public String getImageFileName() {
        return imageFileName;
    }

    public String getImagePath() {
        return imagePath;
    }

    //Getting text 
    public String getCaption() {
        return text;
    }

    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
        imageFileName = initImageFileName;
    }

    public void setImagePath(String initImagePath) {
        imagePath = initImagePath;
    }

    public void setImage(String initPath, String initFileName) {
        imagePath = initPath;
        imageFileName = initFileName;
    }

    public void setText(String newText) {
        text = newText;
    }
}

