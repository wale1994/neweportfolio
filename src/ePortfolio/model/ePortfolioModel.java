/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  @Author: Olawale Onigemo
 */
package ePortfolio.model;

import ePortfolio.component.Component;
import ePortfolio.component.ImageComponent;
import ePortfolio.component.ParagraphComponent;
import ePortfolio.component.VideoComponent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ePortfolio.view.ePortfolioMakerView;

/**
 *
 * @author WaleO
 */
public class ePortfolioModel {

    ePortfolioMakerView ui;
    String title;
    ObservableList<Component> siteComponents;
    Component selectedComponent;
 
 
    int count; //be used to keep track of number of slides. 

    public ePortfolioModel(ePortfolioMakerView initUI) {
        ui = initUI;
        siteComponents = FXCollections.observableArrayList();
        count = 0;
        reset();
    }
    
     // ACCESSOR METHODS
    public boolean isComponentSelected() {
	return selectedComponent != null;
        
    }
    
    public ObservableList<Component> getComponents() {
	return siteComponents;
    }
    
    public Component getSelectedComponent() {
	return selectedComponent;
    }

    public String getTitle() { 
	return title; 
    }
    
    // MUTATOR METHODS
    public void setSelectedComponent(Component initSelectedComponent) {
	selectedComponent = initSelectedComponent;
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }
    
     /**
     * Resets the ePortfolio to have no pages and a default title.
     */
    public void reset() {
	siteComponents.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager(); 
	title = "Enter Title";
	selectedComponent = null;
    }
    
       /**This will compare the component being sent over to the component already selected in
    * ePortfolio.
    * @param aComponent
    * @return true if its a match, false if not.
    */
  
   public boolean testComponent(Component aComponent)
   {
      return selectedComponent.equals(aComponent);
           
       
   }
  
       /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the image to add.
     * @param initImagePath File path for the image to add.
     * @param caption String of the caption to be added with the image.
     */
    public void addImageComponent(   String initImageFileName,
			    String initImagePath, String caption) {
	
        ImageComponent componentToAdd = new ImageComponent(initImageFileName, initImagePath, caption);
	siteComponents.add(componentToAdd);
	ui.reloadEportfolioPane(this);
        count++; //adds to the number of components.
        
    }
    
      
       /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initVideoFileName File name of the image to add.
     * @param initVideoPath File path for the image to add.
     * @param caption String of the caption to be added with the image.
     */
    public void addVideoComponent(   String initVideoFileName,
			    String initVideoPath, String caption) {
	VideoComponent componentToAdd = new VideoComponent(initVideoFileName, initVideoPath, caption);
	siteComponents.add(componentToAdd);
	ui.reloadEportfolioPane(this);
        count++; //adds to the number of components
        
    }
    
    /**
     * Adds the paragraph being sent over to the list. 
     * @param theComponent 
     */
    public void addParapgraphComponent(ParagraphComponent theComponent)
    {
        ParagraphComponent paragraphToAdd = new ParagraphComponent ();
        
        //Get the paragraph stored in the passed paragraph compponent and set it to the 
        //paragraph component being added to the component list. 
        paragraphToAdd.setParagraph(theComponent.getParagraph());
        //Add the paragraph to the list
        siteComponents.add(paragraphToAdd);
        ui.reloadEportfolioPane(this);
        count++;
    }
    
        /**
     * This will remove the slide selected and the selected component ONLY 
     * Turns off selection so that no component is selected after. 
     * @param theComponent
     */
    public void removeComponent(Component theComponent)
    {
        Component componentToDelete = theComponent;
        siteComponents.remove(componentToDelete);
        selectedComponent = null;
        ui.reloadEportfolioPane(this);
        count--; 
    }
    
    public int getCount()
    {
        return count; 
    }
    
    /**
     * Add to the count of the number of components in the list if for some reason
     * Additonal components is added. 
     * 
     */
    public void addToCount()
    {
        count = count + 1;
    }
    
    /**
     * subFromCount - deducts from the count of the number of components in the list. 
     */
    public void subFromCount()
    {
        count = count - 1;
    }
    
    /**
     * getSelectedIndex 
     * Gets the index of the selected component in the list 
     * @return int - represents the location of the selectedComponent in the list.
     */
    public int getSelectedIndex()
    {
        int theIndex = 0;
        theIndex = siteComponents.indexOf(selectedComponent);
        return theIndex; 
    }

}
