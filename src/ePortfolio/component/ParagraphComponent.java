/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.component;

import static eportfoliomaker.ePortfolioConstants.DEFAULT_PARAGRAPH_IMAGE;
import static eportfoliomaker.ePortfolioConstants.PATH_SLIDE_SHOW_IMAGES;

/**
 * This is the class that stores the data necessary for a paragraph component.
 *
 * @author WaleO
 */
public class ParagraphComponent extends Component {

    private String type;
    private String paragraph;
    String paragraphFileName;
    String paragraphImagePath;

    /**
     * Constructor Initilizes instance variables
     */
    public ParagraphComponent() {

        type = "Paragraph";
        paragraph = "Enter Paragraph";
        paragraphFileName = DEFAULT_PARAGRAPH_IMAGE;
        paragraphImagePath = PATH_SLIDE_SHOW_IMAGES;
    }

    public ParagraphComponent(String theString) {
        type = "Paragraph";
        paragraph = theString;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String newParagraph) {
        paragraph = newParagraph;
    }
    
    public String getParagraphFileName()
    {
        return paragraphFileName;
    }
    
    public String getParagraphImagePath()
    {
        return paragraphImagePath;
    }

    @Override
    public String getComponentType() {
        return type;
    }
}
