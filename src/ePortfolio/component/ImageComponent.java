/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.component;

/**
 *
 * @author WaleO
 */
public class ImageComponent extends Component{
    
    String imageFileName;
    String imagePath;
    String caption;
    String type; 
    
        /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * @param initCaption
     * @param initImagePath File path for the image.
     * 
     */
    public ImageComponent(String initImageFileName, String initImagePath, String initCaption)
    {
        imageFileName = initImageFileName;
        imagePath = initImagePath; 
        caption = initCaption;
        type = "Image";
    }
    
    /**
     * getImageFileName 
     * @return String of the path of the image file name.
     */
    public String getImageFileName()
    {
        return imageFileName; 
    }
    
    public String getImagePath()
    {
        return imagePath;
    }
    
    public String getCaption()
    {
        return caption;
    }
    
        // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
   
    public void setCaption(String newText)
    {
        caption = newText;
    }

    @Override
    public String getComponentType() {
        return type; 
    }
    
    
}
