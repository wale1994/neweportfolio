/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.component;

/**
 *
 * @author WaleO
 */
public class Header extends Component {

    private String type;
    private String header;

    /**
     * Constructor 
     * Initilizes instance variables
     */
    public Header() {
        type = "Header";
        header = "Enter Header";
    }
    
    public Header(String theString)
    {
        type = "Header";
        header = theString;
    }
    
    public String getHeader()
    {
        return header;
    }
    
    public void setHeader(String newHeader)
    {
        header = newHeader; 
    }

    @Override
    public String getComponentType() {
        return type;
    }

}
