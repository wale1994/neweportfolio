/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.component;

/**
 *
 * @author WaleO
 */
public class VideoComponent extends Component {

    String videoFileName;
    String videoPath;
    String caption;
    String type;

    /**
     * Constructor, it initializes all slide data.
     *
     * @param initImageFileName File name of the image.
     * @param initCaption Caption of the video (if it was saved)
     * @param initImagePath File path for the image.
     *
     */
    public VideoComponent(String initVideoFileName, String initVideoPath, String initCaption) {
        videoFileName = initVideoFileName;
        videoPath = initVideoPath;
        caption = initCaption;
        type = "Video";
    }

    /**
     * getVideoFileName
     *
     * @return String of the path of the video file name.
     */
    public String getVideoFileName() {
        return videoFileName;
    }

    /**
     * getVideoPath get the path of the video
     * @return videoPath String of the video path 
     */
    public String getVideoPath() {
        return videoPath;
    }

    public String getCaption() {
        return caption;
    }

    // MUTATOR METHODS
    public void setVideoFileName(String initVideoFileName) {
        videoFileName = initVideoFileName;
    }

    public void setVideoPath(String initImagePath) {
        videoPath = initImagePath;
    }

    public void setVideo(String initPath, String initFileName) {
        videoPath = initPath;
        videoFileName = initFileName;
    }

    public void setCaption(String newText) {
        caption = newText;
    }

    @Override
    public String getComponentType() {
         return type;
    }

}
