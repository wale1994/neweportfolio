/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  @Author: Olawale Onigemo
 */
package ePortfolio.file;

import ePortfolio.component.Component;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import static eportfoliomaker.ePortfolioConstants.PATH_SLIDE_SHOWS;

import ePortfolio.model.ePortfolioModel;

/**
 *
 * @author WaleO
 */
public class EportfolioFilerManager {

    // JSON FILE READING AND WRITING CONSTANTS
    public static String JSON_TITLE = "title";
    public static String JSON_HEADER = "header";
    public static String JSON_PAGES = "pages";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public static String JSON_CAPTION = "caption";
    public static String JSON_PARAGRAPH = "paragraph";
    public static String JSON_TEXT = "text";
    public static String JSON_VIDEO = "video";

    /**
     * This method saves all the data associated with a slide show to a JSON
     * file.
     *
     * @param slideShowToSave The course whose data we are saving.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public void saveEportfolio(ePortfolioModel ePortfolioToSave) throws IOException {
        // BUILD THE FILE PATH
        String ePortfolioTitle = "" + ePortfolioToSave.getTitle();
        String jsonFilePath = PATH_SLIDE_SHOWS + SLASH + ePortfolioTitle + JSON_EXT;

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);

        // BUILD THE SLIDES ARRAY
        

        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        //JsonObject courseJsonObject = Json.createObjectBuilder()
             //   .add(JSON_TITLE, ePortfolioToSave.getTitle())
          //      .add(JSON_PAGES, pagesJsonArray)
           //     .build();

        // AND SAVE EVERYTHING AT ONCE
       // jsonWriter.writeObject(courseJsonObject);
    }
    
     private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }  
     
         private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }

  /*  private JsonArray makeComponentJsonArray(List<Component> components) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Component component : components) {
            JsonObject jso = makePageJsonObject(components);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }*/

   /** private JsonObject makePageJsonObject(Page page) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_IMAGE_FILE_NAME, page.getImageFileName())
                .add(JSON_IMAGE_PATH, page.getImagePath())
                .add(JSON_CAPTION, page.getCaption())
                .build();
        return jso;
        */
    }

