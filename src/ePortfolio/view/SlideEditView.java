/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.view;

import ePortfolio.component.SlideShowComponent;
import ePortfolio.controller.ImageSelectionController;
import ePortfolio.dialog.SlideShowDialog;
import static ePortfolio.file.EportfolioFilerManager.SLASH;
import ePortfolio.model.Slide;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_CAPTION_VIEW;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static eportfoliomaker.ePortfolioConstants.DEFAULT_THUMBNAIL_WIDTH;
import static eportfoliomaker.ePortfolioConstants.STYLE_SHEET_UI;
import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption. This will be in UI inside the slideShow component added
 * to the ePortfolio editor page. 
 * 
 * @author McKilla Gorilla & _____________
 * @co_author Olawale Onigemo
 */
public class SlideEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    SlideShowComponent theShow;
    SlideShowDialog theShowView;
    int clickCount = 0;
    //Adding a SlideShowModel object (importing SlideShowModel class that wasnt here before)
   
  
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    HBox imageHBox;
    ImageView imageSelectionView;
    
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     * @param initShow
     * @param view
     */
    public SlideEditView(Slide initSlide, SlideShowComponent initShow, SlideShowDialog view) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
        theShow = initShow;
        theShowView = view;
        
        

	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	updateSlideImage();

	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
        
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	captionTextField = new TextField();
        
       
        captionTextField.setText(slide.getCaption());
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);
        
        //Allows the css to affect the captionTextField where inputs to the captions 
        //are made. 
        captionVBox.getStylesheets().add(STYLE_SHEET_UI);
        captionVBox.getStyleClass().add(CSS_CLASS_CAPTION_VIEW);
        
        
        captionTextField.textProperty().addListener((Observable, oldValue, newValue) -> {
            slide.setText(newValue);
        });

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
        imageHBox = new HBox();
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
        
        captionVBox.setOnMouseClicked(e -> {
          theShow.setSelectedSlide(slide);
          theShowView.reloadSlideShowMakerPane(theShow);
         
	});
      
       
	// SETUP THE EVENT HANDLERS
	imageController = new ImageSelectionController();
        imageSelectionView.setOnMouseClicked(e -> {
             
            theShow.setSelectedSlide(slide);
            imageController.processSlideImageSelector(slide, this);
            theShowView.reloadSlideShowMakerPane(theShow);
	});

    }
     
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	File file = new File(imagePath);
	try {
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    imageSelectionView.setImage(slideImage);
            
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);
            
           
	} catch (Exception e) {
	    // @todo - use Error handler to respond to missing image
	}
    }    
}