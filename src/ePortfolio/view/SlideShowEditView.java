/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.view;

import ePortfolio.component.Component;
import ePortfolio.component.VideoComponent;
import ePortfolio.controller.VideoSelectionController;
import ePortfolio.controller.ePortfolioEditController;
import static ePortfolio.file.EportfolioFilerManager.SLASH;
import ePortfolio.model.ePortfolioModel;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_CAPTION_VIEW;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_PAGE_EDIT_VIEW;
import static eportfoliomaker.ePortfolioConstants.DEFAULT_THUMBNAIL_WIDTH;
import static eportfoliomaker.ePortfolioConstants.STYLE_SHEET_UI;
import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 * This class presents the view for the Video component on the UI. Here is where
 * editing for this particular view occurs.
 *
 * @author Olawale Onigemo
 */
public class SlideShowEditView extends HBox {

    // SLIDE THIS COMPONENT EDITS
    Component theComponent;
    //The ePortfolioModel we are editing the images to.
    ePortfolioModel theEportfolio;
    //The ePortfolioView that changes the UI as it is edited.
    ePortfolioMakerView ePortView;
    ePortfolioEditController theController;

    // DISPLAYS THE IMAGE FOR THIS Component
    HBox imageHBox;
    ImageView imageSelectionView;
    //This label displays the text created by the user;
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;

    public SlideShowEditView() {

        theComponent = null;
        theEportfolio = null;
        ePortView = null;
        //Set the style of the edit view.

        // SETUP THE CAPTION CONTROLS
        captionVBox = null;

        captionLabel = null;
        captionTextField = null;
    }

    public SlideShowEditView(Component initComponent, ePortfolioModel initEportfolio, ePortfolioMakerView initView) {

        theComponent = initComponent;
        theEportfolio = initEportfolio;
        ePortView = initView;
        //Set the style of the edit view.
        this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);

        // SETUP THE CAPTION CONTROLS
        captionVBox = new VBox();

        // Will display a default image of the video. 
        imageSelectionView = new ImageView();
        updateVideo();

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        captionLabel = new Label();
        captionTextField = new TextField();

        // PROVIDES RESPONSES FOR IMAGE SELECTION
        VideoSelectionController videoController;

        captionLabel.setText("Video Caption");
        captionTextField.setText(((VideoComponent) theComponent).getCaption());
        captionVBox.getChildren().add(captionLabel);
        captionVBox.getChildren().add(captionTextField);

        //Allows the css to affect the captionTextField where inputs to the captions 
        //are made. 
        captionVBox.getStylesheets().add(STYLE_SHEET_UI);
        captionVBox.getStyleClass().add(CSS_CLASS_CAPTION_VIEW);

        //Save the added text of the caption field.
        captionTextField.textProperty().addListener((Observable, oldValue, newValue) -> {
            ((VideoComponent) theComponent).setCaption(newValue);
        });

        // LAY EVERYTHING OUT INSIDE THIS COMPONENT
        imageHBox = new HBox();
        getChildren().add(imageSelectionView);
        getChildren().add(captionVBox);

        captionVBox.setOnMouseClicked(e -> {
            theEportfolio.setSelectedComponent(theComponent);
            ePortView.reloadEportfolioPane(theEportfolio);

        });

        // SETUP THE EVENT HANDLERS
        videoController = new VideoSelectionController();

        //When the imageview is clicked the component is selected and 
        //Allows the user to change the image. 
        imageSelectionView.setOnMouseClicked(e -> {

            theEportfolio.setSelectedComponent(theComponent);
            //videoController.processSelectVideo((VideoComponent) theComponent, this);
            ePortView.reloadEportfolioPane(theEportfolio);
        });

    }

    /**
     * This function gets the image for the video and uses it to update the
     * image displayed.
     */
    public void updateVideo() {
        String videoPath = ((VideoComponent) theComponent).getVideoPath() + SLASH + ((VideoComponent) theComponent).getVideoFileName();
        File file = new File(videoPath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image videoImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(videoImage);

            // AND RESIZE IT
            double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
            double perc = scaledWidth / videoImage.getWidth();
            double scaledHeight = videoImage.getHeight() * perc;
            imageSelectionView.setFitWidth(scaledWidth);
            imageSelectionView.setFitHeight(scaledHeight);

        } catch (Exception e) {
            // @todo - use Error handler to respond to missing image
        }
    }

}
