/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.view;

/**
 *
 * @author Olawale Onigemo
 */
import ePortfolio.component.Component;
import ePortfolio.component.ImageComponent;
import ePortfolio.component.SlideShowComponent;
import ePortfolio.controller.FileController;
import ePortfolio.controller.ePortfolioEditController;
import ePortfolio.dialog.ImageDialog;
import ePortfolio.dialog.SlideShowDialog;
import ePortfolio.error.ErrorHandler;
import ePortfolio.file.EportfolioFilerManager;
import java.util.Optional;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_FILE_TOOLBAR_PANE;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static eportfoliomaker.ePortfolioConstants.ICON_ADD_PAGE;
import static eportfoliomaker.ePortfolioConstants.ICON_REMOVE_PAGE;
import static eportfoliomaker.ePortfolioConstants.ICON_EXIT;
import static eportfoliomaker.ePortfolioConstants.ICON_MOVE_DOWN;
import static eportfoliomaker.ePortfolioConstants.ICON_MOVE_UP;
import static eportfoliomaker.ePortfolioConstants.ICON_TITLE;
import static eportfoliomaker.ePortfolioConstants.PATH_ICONS;
import static eportfoliomaker.ePortfolioConstants.STYLE_SHEET_UI;

import ePortfolio.model.ePortfolioModel;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_HIGHLIGHT;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_PAGE_EDIT_VBOX;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_SITE_HORIZONTAl_TOOLBAR_BUTTONS;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_SITE_TOOLBAR_PANE;

import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_TABS;

import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS;
import static eportfoliomaker.ePortfolioConstants.ICON_BANNER;
import static eportfoliomaker.ePortfolioConstants.ICON_COLOR;
import static eportfoliomaker.ePortfolioConstants.ICON_FONT;
import static eportfoliomaker.ePortfolioConstants.ICON_FOOTER;
import static eportfoliomaker.ePortfolioConstants.ICON_H;
import static eportfoliomaker.ePortfolioConstants.ICON_IMAGE;
import static eportfoliomaker.ePortfolioConstants.ICON_LAYOUT;
import static eportfoliomaker.ePortfolioConstants.ICON_LIST;
import static eportfoliomaker.ePortfolioConstants.ICON_LOAD_EPORTFOLIO;
import static eportfoliomaker.ePortfolioConstants.ICON_NAME;
import static eportfoliomaker.ePortfolioConstants.ICON_NEW_EPORTFOLIO;
import static eportfoliomaker.ePortfolioConstants.ICON_PARAGRAPH;
import static eportfoliomaker.ePortfolioConstants.ICON_SAVE_EPORTFOLIO;
import static eportfoliomaker.ePortfolioConstants.ICON_SLIDESHOW;
import static eportfoliomaker.ePortfolioConstants.ICON_VIDEO;
import static eportfoliomaker.ePortfolioConstants.ICON_VIEW_EPORTFOLIO;
import static java.awt.PageAttributes.MediaType.C;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.web.WebView;

public class ePortfolioMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ePortfolioMakerPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    VBox siteToolbarPane;
    HBox pageWorkSpace;
    HBox innerWorkspace; // For the tabbed pane inside the Page Editor Tab.
    VBox pageEditBar;
    VBox pageSite;
    TabPane siteTabPane; //might be used for selecting pages.
    TabPane pagesTab; //The page tab that will appear with editing controls.
    

    HBox iconButtons; // Used for buttons with icons demonstrating an action
    HBox icon2;
    HBox textButtons; // Used for buttons that are for text
    HBox textButtons2;
    HBox textButtons3;
    HBox topColor;

    //These will be used for scrolling for the editing workspace 
    // AND THIS WILL GO IN THE CENTER
    VBox pageEditorPane;
    ScrollPane pageEditorScrollPane;

    //All toolbar related buttons. The following below is for file toolbar
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button exitButton;
    Button changeTitle;

    //Handles any events that will occur for the project.
    //ePortfolioEditController fileController;
    //Sitetoolbar Pageeditor workspace related buttons. 
    Button removePageButton;
    Button addPageButton;

    //These following buttons are for editing components for a page such as text(paragraph), image
    //video, slideshow, header, and lists.
    Button listButton;
    Button paragraphButton;
    Button imageButton;
    Button videoButton;
    Button slideShowButton;
    Button headerButton;
    Button layoutButton;
    Button colorButton;
    Button fontButton;
    Button footerButton;
    Button titleButton;
    Button studentButton;
    Button bannerButton;
    Button removeComponent;
    //Controls for dialog classes 
    ImageDialog imgDialog = new ImageDialog(this);
    SlideShowDialog showDialog = new SlideShowDialog(this); // Creates the slideshow
   

    // THIS IS THE ePortfolio WE'RE WORKING WITH
    ePortfolioModel ePortfolio;
    
    //This is the slideshow we're working with to this ePortfolio
    SlideShowComponent slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    EportfolioFilerManager fileManager;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    //private FileController fileController;
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private ePortfolioEditController editController;

    private ErrorHandler errorHandler;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public ePortfolioMakerView(EportfolioFilerManager initFileManager) {
        // FIRST HOLD ONTO THE FILE MANAGER
        fileManager = initFileManager;

        // MAKE THE DATA MANAGING MODEL
        ePortfolio = new ePortfolioModel(this);

    }

    // ACCESSOR METHODS
    public ePortfolioModel getEportfolio() {
        return ePortfolio;
    }
    
    public SlideShowComponent getSlideShow()
    {
        //Add code returning slideshow variable 
        return slideShow;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    public Button initChildButton(Pane toolbar, String iconFileName, String cssClass,
            boolean disabled) {

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        toolbar.getChildren().add(button);
        return button;
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_FILE_TOOLBAR_PANE);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_EPORTFOLIO, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);

    }

    private void initSiteToolbar() {

        siteToolbarPane = new VBox();
        siteToolbarPane.getStyleClass().add(CSS_CLASS_SITE_TOOLBAR_PANE);

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        addPageButton = this.initChildButton(siteToolbarPane, ICON_ADD_PAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //Placing remove Button/Upslide/DownSlide/View button on toolbar
        removePageButton = this.initChildButton(siteToolbarPane, ICON_REMOVE_PAGE, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    private void initEditWorkspace() {
        pageEditBar = new VBox();
        textButtons = new HBox();
        textButtons2 = new HBox();
        textButtons3 = new HBox();
        icon2 = new HBox();
        iconButtons = new HBox();
        topColor = new HBox();

        pageEditBar.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VBOX);
        videoButton = initChildButton(iconButtons, ICON_VIDEO, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        slideShowButton = initChildButton(iconButtons, ICON_SLIDESHOW, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        colorButton = initChildButton(iconButtons, ICON_COLOR, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        layoutButton = initChildButton(icon2, ICON_LAYOUT, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        imageButton = initChildButton(icon2, ICON_IMAGE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        listButton = initChildButton(icon2, ICON_LIST, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);


        headerButton = initChildButton(textButtons, ICON_H, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        fontButton = initChildButton(textButtons, ICON_FONT, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        paragraphButton = initChildButton(textButtons, ICON_PARAGRAPH, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        footerButton = initChildButton(textButtons2, ICON_FOOTER, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        titleButton = initChildButton(textButtons2, ICON_TITLE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        bannerButton = initChildButton(textButtons2, ICON_BANNER, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        studentButton = initChildButton(textButtons3, ICON_NAME, CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);
        removeComponent = initChildButton(textButtons3,"Remove", CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS, false);

        pageEditBar.getChildren().addAll(topColor, textButtons, iconButtons, icon2, textButtons2, textButtons3);


    }

    private void initPageWorkspace() {
        pageWorkSpace = new HBox();
        innerWorkspace = new HBox();
        VBox webContent = new VBox();

        initEditWorkspace();

        pagesTab = new TabPane();
        Tab page = new Tab("Page#: ");

        //Sets up the scrollbar for the editing pane.
        pageEditorPane = new VBox();
        pageEditorPane.setPrefHeight(1000.0);
        pageEditorPane.setPrefWidth(1000.0);
        pageEditorScrollPane = new ScrollPane(pageEditorPane);
        innerWorkspace.getStyleClass().add(CSS_CLASS_TABS);

        // Tab pagePreview = new Tab("Page#: ");
        page.setContent(pageEditorScrollPane);
        pagesTab.getTabs().add(page);

        WebView web = new WebView();
        web.getEngine().load("http://www.gamespot.com/");

        siteTabPane = new TabPane();
        Tab editorTab = new Tab("Page Editor");
        Tab siteTab = new Tab("Page Preview");
        siteTabPane.getTabs().add(editorTab);
        editorTab.setContent(innerWorkspace);
        siteTab.setContent(web);
        siteTabPane.getTabs().add(siteTab);

        innerWorkspace.getChildren().add(pageEditBar);
        //innerWorkspace.getChildren().add(pageEditorPane);
        //innerWorkspace.getChildren().add(pageEditorScrollPane);
        innerWorkspace.getChildren().add(pagesTab);
        pageWorkSpace.getChildren().add(siteTabPane);

    }

    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        ePortfolioMakerPane = new BorderPane();
        ePortfolioMakerPane.setTop(fileToolbarPane);
        ePortfolioMakerPane.setLeft(siteToolbarPane);
        ePortfolioMakerPane.setCenter(pageWorkSpace);
        primaryScene = new Scene(ePortfolioMakerPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);

        //This will change the icon of the stage.
        //When testing anywhere, internet connection is needed to see this change.
        primaryStage.getIcons().add(new Image("https://d1yn1kh78jj1rr.cloudfront.net/thumbs1/fire-glyph-icon_GkYlVaUu.jpg"));
        primaryStage.show();
    }

    private void initEventHandlers() {
        editController = new ePortfolioEditController(this);
        headerButton.setOnAction(e -> {
            editController.processHeaderRequest();
        });

        fontButton.setOnAction(e -> {
            editController.processFont();
        });

        imageButton.setOnAction(e -> {
            editController.processImageRequest();
        });

        paragraphButton.setOnAction(e -> {
            editController.processParagraph();
        });

        slideShowButton.setOnAction(e -> {
            showDialog.processMakeSlideShow();
        });

        listButton.setOnAction(e -> {
            editController.processList();
        });

        videoButton.setOnAction(e -> {
            editController.processVideo();
        });
        
        removeComponent.setOnAction(e -> {
            editController.processRemove();
        });
    }

    /**
     * Initializes the UI controls and gets it rolling.
     *
     * @param initPrimaryStage The window for this application.
     *
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
        // THE TOOLBAR ALONG THE NORTH
        initFileToolbar();

        //Site page toolbar under the filetoolbar
        initSiteToolbar();

        //The bar for the page editing section 
        initPageWorkspace();

        //For event handlers 
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
    }
    
     /**
     * Uses the ePortfolio data to reload all the components for slide editing.
     *
     * @param ePortfolioToLoad
     */
    public void reloadEportfolioPane(ePortfolioModel ePortfolioToLoad) {
        int i = 0;
        TextEditView textEditor = new TextEditView();
        ImageEditView imageEditor = new ImageEditView();
        VideoEditView videoEditor = new VideoEditView();
        
        pageEditorPane.getChildren().clear();
        for (Component component : ePortfolioToLoad.getComponents()) {
            ePortfolio = ePortfolioToLoad;
            //If the sent component is a Text, add the text 
            if(component.getComponentType().equals("Header") || (component.getComponentType().equals("Paragraph")))
            {
              //Add textEditor component to the editorPane
              textEditor = new TextEditView(component, ePortfolio, this);
              pageEditorPane.getChildren().add(textEditor);
            }
            else 
                if(component.getComponentType().equals("Image"))
                {
                     //Add imageEdit component to the editorpane
                imageEditor = new ImageEditView(component,ePortfolio,this );
                pageEditorPane.getChildren().add(imageEditor); 
                }
                else 
                {
                    videoEditor = new VideoEditView(component,ePortfolio,this );
                    pageEditorPane.getChildren().add(videoEditor);
                }
       
            /**
             * Compares the slide to the selected component. Will rehighlight the
             * selected component if it is true First we must check to see if a
             * component in the ePortofolio has been selected.
             */
            if (ePortfolio.isComponentSelected()) {
                if (ePortfolio.testComponent(component)) {
                    ePortfolio.setSelectedComponent(component);
           
                    if((component.getComponentType().equals("Header")) || 
                        (component.getComponentType().equals("Paragraph")))  
                    {
                    textEditor.getStyleClass().add(CSS_CLASS_HIGHLIGHT);
                    System.out.println("This is seleceted");
                    }
                    else 
                        imageEditor.getStyleClass().add(CSS_CLASS_HIGHLIGHT);
                   // removeSlideButton.setDisable(false);
                    //upSlideButton.setDisable(false);
                    //downSlideButton.setDisable(false);
                }

            } 
        }       
    }


}
