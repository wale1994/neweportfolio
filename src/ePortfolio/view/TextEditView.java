/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.view;

import ePortfolio.component.Component;
import ePortfolio.component.Header;
import ePortfolio.component.ImageComponent;
import ePortfolio.component.ParagraphComponent;
import ePortfolio.controller.ePortfolioEditController;
import ePortfolio.dialog.HeaderDialog;
import ePortfolio.dialog.ParagraphDialog;
import static ePortfolio.file.EportfolioFilerManager.SLASH;
import ePortfolio.model.ePortfolioModel;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_CAPTION_VIEW;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_PAGE_EDIT_VIEW;
import static eportfoliomaker.ePortfolioConstants.DEFAULT_THUMBNAIL_WIDTH;
import static eportfoliomaker.ePortfolioConstants.STYLE_SHEET_UI;
import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author WaleO
 */
public class TextEditView extends HBox {

    // SLIDE THIS COMPONENT EDITS
    Component theComponent;
    ePortfolioModel theEportfolio;
    ePortfolioMakerView ePortView;
    ePortfolioEditController theController;
  

    // DISPLAYS THE IMAGE FOR THIS Component
    HBox imageHBox;
   
    ImageView imageSelectionView;
    VBox captionVBox;
    Label captionLabel;

    public TextEditView() {
        theComponent = null;
        theEportfolio = null;

        ePortView = null;
        theController = null;
       

        // SETUP THE CAPTION CONTROLS
        captionVBox = null;

        captionLabel = null;
    }

    /**
     * THis constructor initializes the full UI for this component, using the
     * initSlide data for initializing values./
     *
     * @param initComponent //The text component being edited
     * @param initEportfolio // The ePortfolio being passed to be edited
     * @param view //The view to be edited to display the changes.
     *
     */
    public TextEditView(Component initComponent, ePortfolioModel initEportfolio, ePortfolioMakerView view) {
        // FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
        this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);

        // KEEP THE SLIDE FOR LATER
        theComponent = initComponent;
        theEportfolio = initEportfolio;
        imageSelectionView = new ImageView();
        

        ePortView = view;
        theController = new ePortfolioEditController(ePortView);
      

        // SETUP THE CAPTION CONTROLS
        captionVBox = new VBox();

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        captionLabel = new Label();

        captionVBox.getChildren().add(captionLabel);

        //Allows the css to affect the captionTextField where inputs to the captions 
        //are made. 
        captionVBox.getStylesheets().add(STYLE_SHEET_UI);
        captionVBox.getStyleClass().add(CSS_CLASS_CAPTION_VIEW);

        // LAY EVERYTHING OUT INSIDE THIS COMPONENT
        imageHBox = new HBox();
        imageHBox.getChildren().add(captionVBox);

        //If the component is a Header make a Text Edit View WITHOUT A Image in the view
        //If its a paragraph, or List component make the TextEditView have a Image on it.
        if(theComponent.getComponentType().equals("Header"))
        {
             getChildren().addAll(captionLabel, captionVBox, imageHBox);
        }
        else 
        {
            updateParagraphImage();
            getChildren().addAll(imageSelectionView, captionVBox, imageHBox);
        }
       
        
        //Change the Label accordingly if the Text edit view is a paragraph, header or list.
          if (theComponent.getComponentType().equals("Header")) {
            captionLabel.setText("Header: " + ((Header) theComponent).getHeader());
        }
        else 
            if(theComponent.getComponentType().equals("Paragraph"))
                captionLabel.setText("Paragraph");

        this.setOnMouseClicked(e -> {
            if (theComponent.getComponentType().equals("Header")) {
                HeaderDialog newHeader = new HeaderDialog();
                theEportfolio.setSelectedComponent(theComponent);
                newHeader.editHeader(captionLabel.getText().substring(8), ePortView);

                ePortView.reloadEportfolioPane(theEportfolio);
            }
            else 
                if(theComponent.getComponentType().equals("Paragraph"))
                {
                   ParagraphDialog newParagraph = new ParagraphDialog();
                   String theText = ((ParagraphComponent)theComponent).getParagraph();
                   theEportfolio.setSelectedComponent(theComponent);
                   newParagraph.editParagraph(theText, view);
                   ePortView.reloadEportfolioPane(theEportfolio);
                }


        });

    }

    /**
     * This will update any text component content
     * @param newText The text being sent over to update the Header
     */
    public void updateText(String newText) {
        if (theComponent.getComponentType().equals("Header")) {
            //Add dialog method for header here. 
        }
    }
    
    public void updateParagraphImage() {
       
        //This will get the default image for a paragraph edit view component 
        String imagePath = ((ParagraphComponent)theComponent).getParagraphImagePath()+ SLASH + 
                           ((ParagraphComponent)theComponent).getParagraphFileName();
        
        //Send the path to the image to the file. 
        File file = new File(imagePath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image paragraphImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(paragraphImage);

            // AND RESIZE IT
            double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
            double perc = scaledWidth / paragraphImage.getWidth();
            double scaledHeight = paragraphImage.getHeight() * perc;
            imageSelectionView.setFitWidth(scaledWidth);
            imageSelectionView.setFitHeight(scaledHeight);

        } catch (Exception e) {
            // @todo - use Error handler to respond to missing image
        }
    }

}
