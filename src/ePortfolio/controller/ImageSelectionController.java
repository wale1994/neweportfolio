/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.controller;

import ePortfolio.component.Component;
import ePortfolio.component.ImageComponent;
import ePortfolio.model.Slide;
import ePortfolio.view.ImageEditView;
import ePortfolio.view.SlideEditView;
import static eportfoliomaker.ePortfolioConstants.PATH_SLIDE_SHOW_IMAGES;
import java.io.File;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;

/**
 * This class is responsible for responding to on click events on the
 * imageEditView to then allow the user to browse for image selections.
 *
 * @author Olawale Onigemo
 */
public class ImageSelectionController {

    /**
     * Default constructor doesn't need to initialize anything
     */
    public ImageSelectionController() {
    }

    /**
     * This function provides the response to the user's request to select an
     * image.
     *
     * @param componentToEdit
     *
     * @param view The user interface control group where the image will appear
     * after selection.
     */
    public void processSelectImage(ImageComponent componentToEdit, ImageEditView view) {
        FileChooser imageFileChooser = new FileChooser();

        //.indexOf(slideToEdit);
        //slideToEdit.
        // SET THE STARTING DIRECTORY
        imageFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));

        // LET'S ONLY SEE IMAGE FILES
        FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);

        // LET'S OPEN THE FILE CHOOSER
        File file = imageFileChooser.showOpenDialog(null);
        if (file != null) {
            String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
            String fileName = file.getName();
            componentToEdit.setImage(path, fileName);
            view.updateImage();
        } else {
            // @todo provide error message for no files selected
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("File Loading Error");
            alert.setContentText("File could not be open!");

            alert.showAndWait();
        }

    }

    public void processSlideImageSelector(Slide slideToEdit, SlideEditView view) {
        FileChooser imageFileChooser = new FileChooser();

        // SET THE STARTING DIRECTORY
        imageFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOW_IMAGES));

        // LET'S ONLY SEE IMAGE FILES
        FileChooser.ExtensionFilter jpgFilter = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter pngFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        FileChooser.ExtensionFilter gifFilter = new FileChooser.ExtensionFilter("GIF files (*.gif)", "*.GIF");
        imageFileChooser.getExtensionFilters().addAll(jpgFilter, pngFilter, gifFilter);

        // LET'S OPEN THE FILE CHOOSER
        File file = imageFileChooser.showOpenDialog(null);
        if (file != null) {
            String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
            String fileName = file.getName();
            slideToEdit.setImage(path, fileName);
            view.updateSlideImage();
        } else {
            // @todo provide error message for no files selected
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("File Loading Error");
            alert.setContentText("File could not be open!");

            alert.showAndWait();
        }

    }
}
