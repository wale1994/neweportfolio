/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
   @author Olawale Onigemo
 */
package ePortfolio.controller;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

import static eportfoliomaker.ePortfolioConstants.PATH_DATA;
import ePortfolio.model.ePortfolioModel;
import ePortfolio.error.ErrorHandler;
import ePortfolio.file.EportfolioFilerManager;
import ePortfolio.view.ePortfolioMakerView;
import static eportfoliomaker.ePortfolioConstants.PATH_SLIDE_SHOWS;
import static eportfoliomaker.ePortfolioConstants.STYLE_SHEET_UI;
import static ePortfolio.file.EportfolioFilerManager.JSON_EXT;
import static ePortfolio.file.EportfolioFilerManager.SLASH;


/**
 *
 * @author WaleO
 */
public class FileController {
    
      // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private ePortfolioMakerView ui;

    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private EportfolioFilerManager ePortfolioIO;



    private int index;
    private String imagePath;
    private File file;
    private Image slideImage;
    private URL fileURL;
    private ImageView theImage;
    private Object dialog;
    private Label captionLabel;
    private String[] imageFileNames;
    private String slidesImageName;

}
