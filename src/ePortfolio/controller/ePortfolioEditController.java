/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @author Olawale Onigemo

 */
package ePortfolio.controller;

import ePortfolio.component.Header;
import ePortfolio.component.ParagraphComponent;
import properties_manager.PropertiesManager;

//import static ssm.StartupConstants.CSS_CLASS_CAPTION_VIEW;
import static eportfoliomaker.ePortfolioConstants.DEFAULT_SLIDE_IMAGE;
import ePortfolio.model.ePortfolioModel;
import ePortfolio.view.ePortfolioMakerView;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_DIALOG;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_VERTICALBAR;
import static eportfoliomaker.ePortfolioConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS;
import static eportfoliomaker.ePortfolioConstants.DEFAULT_VIDEO_IMAGE;

import static eportfoliomaker.ePortfolioConstants.ICON_ADD_PAGE;
import static eportfoliomaker.ePortfolioConstants.ICON_NEXT;
import static eportfoliomaker.ePortfolioConstants.ICON_PREVIOUS;
import static eportfoliomaker.ePortfolioConstants.ICON_REMOVE_PAGE;
import static eportfoliomaker.ePortfolioConstants.PATH_SLIDE_SHOW_IMAGES;
import static eportfoliomaker.ePortfolioConstants.PATH_VIDEOS;
import static eportfoliomaker.ePortfolioConstants.STYLE_SHEET_UI;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author WaleO
 */
public class ePortfolioEditController {

    public ePortfolioMakerView ui;
    private HBox content;
    private Button addText;
    private Button okButton;
    private Button cancel;
    private Button removeText;
    private Button newRemove;
    private VBox fieldBox;
    private int count;
    private String newText;
    Header newHeader;
    ParagraphComponent newParagraph;

    /**
     * This constructor keeps the UI for later.
     */
    public ePortfolioEditController(ePortfolioMakerView initUI) {
        ui = initUI;
        content = new HBox();
        addText = new Button();
        removeText = new Button();
        okButton = new Button();
        cancel = new Button();
        newRemove = new Button();
        count = 0;
        newText = new String();
        newHeader = new Header();
    }

    /**
     * Provides a response for when the user wishes to add a new slide to the
     * slide show.
     */
    public void processHeaderRequest() {
        //This code will bring up the pop up combo box for the language selection. 

        newHeader = new Header(enterHeader());
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //Add this to the list of components to the ePortfolio model.
        ui.getEportfolio().getComponents().add(newHeader);
        ui.getEportfolio().addToCount();
        ui.reloadEportfolioPane(ui.getEportfolio());

    }

    public String enterHeader() {
        //The following code will prompt the use to change text.
        TextInputDialog dialog = new TextInputDialog("Edit Header");
        dialog.setTitle("Edit Header");
        dialog.setHeaderText("Header");
        dialog.setContentText("Please enter header text:"); // Where the user puts input

        //If the file is already saved the user wont need to enter a title again.
        Optional<String> result = dialog.showAndWait();
        return result.get();
    }

    public void processFont() {
        List<String> choices = new ArrayList<>();
        choices.add("Header");
        choices.add("Paragrah");
        choices.add("List");

        ChoiceDialog<String> dialog = new ChoiceDialog<>("Header", choices);
        dialog.setTitle("Text");
        dialog.setHeaderText("Font Options");
        dialog.setContentText("Choose the text component you want to change.");

        DialogPane dialogPane = dialog.getDialogPane();

        //Adds the style sheet necessary to apply css to the regions of the 
        //dialog box.
        dialogPane.getStylesheets().add(STYLE_SHEET_UI);
        dialogPane.getStyleClass().add(CSS_CLASS_DIALOG);

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
    }

    public void processList() {
        //Allows the user to enter text in the form of a paragraph. 
        TextField txtField = new TextField();
        content = new HBox();
        fieldBox = new VBox();
        addText = new Button();
        removeText = new Button();
        addText.setText("Add");
        removeText.setText("Remove");

        content.getChildren().add(txtField);
        content.getChildren().add(removeText);
        content.getChildren().add(addText);
        fieldBox.getChildren().add(content);

        //Handles the adding of list elements. 
        addText.setOnAction(e -> {
            HandleAddText();
        });

        removeText.setOnAction(e -> {
            HandleRemove();
        });

        Stage stage = new Stage();
        stage.setTitle("List");
        Scene scene = new Scene(fieldBox, 300, 300);
        stage.setScene(scene);
        stage.show();

    }

    public void processVideo() {
        ePortfolioModel ePortfolio = ui.getEportfolio();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ePortfolio.addVideoComponent(DEFAULT_VIDEO_IMAGE, PATH_SLIDE_SHOW_IMAGES, "");
    }

    public void processImageSize() {
        //The following code will prompt the use to change text.
        TextInputDialog dialog = new TextInputDialog("Edit Image");
        dialog.setTitle("Image Size");
        dialog.setHeaderText("Width");
        dialog.setContentText("Please width size:"); // Where the user puts input

        DialogPane dialogPane = dialog.getDialogPane();

        //Adds the style sheet necessary to apply css to the regions of the 
        //dialog box.
        dialogPane.getStylesheets().add(STYLE_SHEET_UI);
        dialogPane.getStyleClass().add(CSS_CLASS_DIALOG);
        //If the file is already saved the user wont need to enter a title again.
        Optional<String> result = dialog.showAndWait();

        //The following code will prompt the use to change text.
        TextInputDialog newDialog = new TextInputDialog("Edit Image");
        dialog.setTitle("Image Size");
        dialog.setHeaderText("Height");
        dialog.setContentText("Please height size:"); // Where the user puts input

        //If the file is already saved the user wont need to enter a title again.
        dialog.showAndWait();
    }

    public void processImageRequest() {

        ePortfolioModel ePortfolio = ui.getEportfolio();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ePortfolio.addImageComponent(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES, "");

    }

    public void processParagraph() {

//        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//        alert.setTitle("Paragraph Options");
//        alert.setHeaderText("Paragraph Options");
//        alert.setContentText("Do you want to add, remove, or edit a Paragraph?");
//        
//        DialogPane dialogPane = alert.getDialogPane();
//        ButtonType addButton = new ButtonType("Add");
//        ButtonType removeButton = new ButtonType("Remove");
//        ButtonType editButton = new ButtonType("Edit");
//        
//        alert.getButtonTypes().setAll(addButton, removeButton, editButton);
//        Optional<ButtonType> result = alert.showAndWait();
//        if (result.get() == addButton) {
//            processParagraphStage();
//        }
        newParagraph = new ParagraphComponent();
        newParagraph.setParagraph(processNewParagraph());
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //Add this to the list of components to the ePortfolio model.
        ui.getEportfolio().addParapgraphComponent(newParagraph);
        ui.reloadEportfolioPane(ui.getEportfolio());

    }

    public String processNewParagraph() {
        //Allows the user to enter text in the form of a paragraph. 
        TextArea text = new TextArea();
        HBox buttonBox = new HBox();
        okButton = new Button();
        cancel = new Button();
        okButton.setText("Ok");
        cancel.setText("Cancel");

        //Display the enter paragraph as a hyperlink
        Button display = new Button("Link all");
        Button someText = new Button("Select Text");

        display.setOnAction(e -> {
            Stage stage2 = new Stage();
            newText = text.getText();

            stage2.setTitle("HyperLinked Edit");
            stage2.setWidth(200);
            stage2.setHeight(200);
            Scene scene = new Scene(new Group());
            VBox root = new VBox();
            Hyperlink link2 = new Hyperlink(newText);

            stage2.setScene(scene);
            stage2.show();
            newText = text.getText();

            stage2.setTitle("Paragraph Editing");
            stage2.setWidth(500);
            stage2.setHeight(500);

            root.getChildren().addAll(link2);
            scene.setRoot(root);

            stage2.setScene(scene);
            stage2.show();

        });

        buttonBox.getChildren().add(display);
        buttonBox.getChildren().add(okButton);
        buttonBox.getChildren().add(cancel);
        BorderPane pane = new BorderPane();
        pane.setCenter(text);
        pane.setBottom(buttonBox);
        Stage stage = new Stage();
        stage.setTitle("Paragraph");
        Scene scene = new Scene(pane, 200, 220);
        stage.setScene(scene);

        stage.show();

        okButton.setOnAction(e -> {
            newText = text.getText();
            stage.close();
        });
        
        cancel.setOnAction(e -> {
            newText = text.getText();
            stage.close();
        });
        return newText;
    }


    private void HandleAddText() {
        count = 0;

        addText.setOnAction(e -> {
            TextField newField = new TextField();
            newRemove = new Button();
            content = new HBox();
            count++;

            //newRemove.setText("Remove");
            content.getChildren().add(newField);
            //content.getChildren().add(newRemove);
            fieldBox.getChildren().add(content);
        });

    }

    private void HandleRemove() {
        count = 0;
        if (fieldBox.getChildren().size() > 0) {
            count = fieldBox.getChildren().size() - 1;
        } else {
            count = 0;
        }

        if (count > 0) {
            fieldBox.getChildren().remove(count);
            count--;
        }

    }

    public void processRemove() {
        ePortfolioModel ePortfolio = ui.getEportfolio();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ePortfolio.removeComponent(ePortfolio.getSelectedComponent());
    }

    public void processHyperLink() {

    }

}
