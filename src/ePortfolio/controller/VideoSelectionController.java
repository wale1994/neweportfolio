/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ePortfolio.controller;

import ePortfolio.component.ImageComponent;
import ePortfolio.component.VideoComponent;
import ePortfolio.view.ImageEditView;
import ePortfolio.view.VideoEditView;
import static eportfoliomaker.ePortfolioConstants.PATH_SLIDE_SHOW_IMAGES;
import static eportfoliomaker.ePortfolioConstants.PATH_VIDEOS;
import java.io.File;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;

/**
 * This class is responsible for responding to on click events on the
 * imageEditView to then allow the user to browse for image selections.
 *
 * @author Olawale Onigemo
 */
public class VideoSelectionController {

    /**
     * Default constructor doesn't need to initialize anything
     */
    public VideoSelectionController() {
    }

    /**
     * This function provides the response to the user's request to select an
     * image.
     *
     * @param componentToEdit
     *
     * @param view The user interface control group where the image will appear
     * after selection.
     */
    public void processSelectVideo(VideoComponent componentToEdit, VideoEditView view) {
        FileChooser videoFileChooser = new FileChooser();

        //.indexOf(slideToEdit);
        //slideToEdit.
        // SET THE STARTING DIRECTORY
        videoFileChooser.setInitialDirectory(new File("./"));

        // LET'S ONLY SEE IMAGE FILES
        FileChooser.ExtensionFilter mp4Filter = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
        FileChooser.ExtensionFilter mp3Filter = new FileChooser.ExtensionFilter("PNG files (*.mp3)", "*.MP3");
        FileChooser.ExtensionFilter wavFilter = new FileChooser.ExtensionFilter("GIF files (*.wav)", "*.W");
        videoFileChooser.getExtensionFilters().addAll(mp4Filter, mp3Filter, wavFilter);

        // LET'S OPEN THE FILE CHOOSER
        File file = videoFileChooser.showOpenDialog(null);
        if (file != null) {
            String path = file.getPath().substring(0, file.getPath().indexOf(file.getName()));
            String fileName = file.getName();
        }
    }

}
