/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  @Author Olawale Onigemo
 */
package eportfoliomaker;

import ePortfolio.error.ErrorHandler;
import ePortfolio.file.EportfolioFilerManager;
import javafx.application.Application;
import xml_utilities.InvalidXMLFileFormatException;
import javafx.stage.Stage;
import ePortfolio.view.ePortfolioMakerView;
import static eportfoliomaker.ePortfolioConstants.PATH_DATA;
import properties_manager.PropertiesManager;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author WaleO
 */
public class EPortfolioMaker extends Application {
   
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    EportfolioFilerManager fileManager = new EportfolioFilerManager();
    String language = new String();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    ePortfolioMakerView ui = new ePortfolioMakerView(fileManager);
    
    @Override
    public void start(Stage primaryStage) throws Exception {
         // LOAD APP SETTINGS INTO THE GUI AND START IT UP

        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = "New ePortfolio";

            // NOW START THE UI IN EVENT HANDLING MODE
            ui.startUI(primaryStage, appTitle);
        } // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
        else {
            // LET THE ERROR HANDLER PROVIDE THE RESPONSE
            //ErrorHandler errorHandler = ui.getErrorHandler();
            //errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, "Loading Error", "File Could not be loaded");
            System.exit(0);
        }
    }
    
 public boolean loadProperties() {
        // SOMETHING WENT WRONG INITIALIZING THE XML FILE
        // if (language.equals(new String("English"))) {
        //eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "Slide Show Error", "XML File could not be initialized");
        // } else {
        //eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, ERROR_TITLESP, XML_ERRORSP);
        // }
        
     PropertiesManager props = PropertiesManager.getPropertiesManager();
     props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
     return true;
 }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
