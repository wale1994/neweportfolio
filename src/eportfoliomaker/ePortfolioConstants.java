/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  @Author Olawale Onigemo
 */
package eportfoliomaker;

/**
 *
 * @author WaleO
 */
public class ePortfolioConstants {
   
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_SLIDE_SHOWS = PATH_DATA + "slide_shows/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_VIDEOS = "./videos/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_CSS = "epm/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "ePortfolioStyle.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_NEW_EPORTFOLIO = "New.png";
    public static String ICON_LOAD_EPORTFOLIO = "Load.png";
    public static String ICON_SAVE_EPORTFOLIO = "Save.png";
    public static String ICON_VIEW_EPORTFOLIO = "View.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_PAGE = "Add.png";
    public static String ICON_REMOVE_PAGE = "Remove.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";
    public static String ICON_TITLE = "Title.png";
    public static String ERROR_TITLESP = "Presentación de diapositivas de error";
    public static String XML_ERRORSP = "XML File could not be initialized";
    
    //Adding constants for controls such as Editing Text, Paragraph, Image, Video, and Slideshow 
    public static String ICON_NAME = "name.png";
    public static String ICON_PARAGRAPH = "paragraph.png";
    public static String ICON_VIDEO = "video.png";
    public static String ICON_IMAGE = "image.png";
    public static String ICON_SLIDESHOW ="slideshow.png";
    public static String ICON_LIST = "list.png";
    public static String ICON_LAYOUT = "layout.jpg";
    public static String ICON_COLOR = "color.jpg";
    public static String ICON_FONT = "font.png";
    public static String ICON_SBU = "sbu.png";
    public static String ICON_H = "h.png";
    public static String ICON_FOOTER = "footer.png";
    public static String ICON_BANNER = "Banner.png";
   
    
    

    // @Standard size for the ePortfolio. May need to add for video as well
    public static String    DEFAULT_SLIDE_IMAGE = "DefaultStartSlide.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 500;
    public static String    DEFAULT_VIDEO_IMAGE = "DefaultStartVideo.png";
    public static String    DEFAULT_PARAGRAPH_IMAGE = "DefaultParagraph.png";
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    
    //The following Style Sheets below are for horizontal toolbars 
    public static String    CSS_CLASS_FILE_TOOLBAR_PANE = "file_toolbar_pane";
    public static String    CSS_CLASS_SITE_TOOLBAR_PANE = "site_toolbar_pane";

    public static String    CSS_CLASS_VERTICALBAR = "vertical_toolbar";
    
    
   //The following CSS are for toolbar buttons of Filetoolbar and SiteToolbar.
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_SITE_HORIZONTAl_TOOLBAR_BUTTONS = "site_toolbar_button";
    public static String    CSS_CLASS_BUTTON_HORIZONTAl_TOOLBAR_TITLE = "button_title";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTONS = "vertical_buttons";
    
    //The following CSS are for components for a slideshow 
    public static String    CSS_CLASS_PAGE_EDIT_VBOX = "page_edit_vbox";
    public static String    CSS_CLASS_PAGE_EDIT_VIEW = "page_edit_view";
    public static String    CSS_CLASS_CAPTION_VIEW = "caption_text";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_HIGHLIGHT = "highlight";
    
    //The following below are for video
    public static String    CSS_CLASS_VIDEO_EDIT_VIEW = "video_edit_view";
    public static String    CSS_CLASS_VIDEO_CAPTION_VIEW = "caption_text";
    public static String    CSS_CLASS_DIALOG = "dialog-pane";
    
    //Style for tabs 
    public static String    CSS_CLASS_TABS = "tab";
    
    // UI LABELS
    public static String    LABEL_SLIDE_SHOW_TITLE = "slide_show_title";
}
